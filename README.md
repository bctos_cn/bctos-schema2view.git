# 可视化管理表结构

## 注意事项

本插件的可视化编辑界面是由uni-app开发，目前无法直接编译到插件里，目前它运行在由uniCloud提供的`前端网页托管`中，所以无法在无网络的情况下使用。
（PS：如果后续哪位能指导下直接在插件里运行uni-app，我将感激不尽）

## 插件由来

最近第一次使用uniCloud开发一个微信公众号项目（另见：[使用dcloud全家桶开发公众号H5系统](http://bctos.cn/doc/18 "使用dcloud全家桶开发公众号H5系统")），感受到云开发带来的便利和低成本。

其中uniCloud中提供的schema2code也是逆天的存在，好用得不行不行的，它能把你的数组表结构（schema）直接转成表单和列表页面，非常省事。

但schema2code需要的表结构（schema）还是需要手工自己写，对于追求快捷，直观的开发者来说还是有点麻烦。

于是本着人人为我，我为人人的原则。于是我打造了这款好用又高效的云数据表结构可视化编辑插件，给自己也给广大uniCloud开发者使用。

## 功能演示

下载插件并重启HBuilder X后，无需任何配置，点开云数据库的表结构文件（如：uniCloud/database/****.schema.json），在编辑器里右键选择`可视化编辑`即可直接进入可视化界面。

![进入可视化编辑](https://caiwu.bctos.cn/other/bctos-schema2view-inter.jpg)

如下图，最上面是数据表权限设置，下面是字段设置，包括字段新增，编辑或者删除等操作，其中单击字段名会进入编辑模式，双击字段名会弹出确认删除字段的提示

更逆天的是还支持上下拖动字段进行排序，排序后的顺序就是后面schema2code生成表单时字段显示的顺序，方便又实用。

![可视化界面](https://caiwu.bctos.cn/other/bctos-schema2view-demo.png)

演示视频（最好全屏播放，缩小显示会比较渣，看不清）

<video id="video" controls="" preload="none" poster="https://caiwu.bctos.cn/other/bctos-schema2view-demo.png">
      <source id="mp4" src="https://caiwu.bctos.cn/other/bctos-schema2view.mp4" type="video/mp4">
</video>

如果视频播放不了可直接在浏览器里打开：[视频地址](https://caiwu.bctos.cn/other/bctos-schema2view.mp4)


## 后续开发计划

第一版本实现了大部分表结构的字段管理功能，还有少部分功能还待增加，如

关联字段（foreignKey）

同一个数据表内父级的字段（parentKey）

还有，不知道是不是HBuilder X插件的问题，在开发调试时Ctrl+s快捷键是可以保存文档的，但正式使用却不可以，只能点左上角保存图标进行保存文档

## 关于我们

从事微信公众号，小程序开发多年，之前主要的技术框架是PHP+uni-app，目前打算去掉PHP后端，直接使用dcloud系列产品实现一体化产品。

如需要合作或定制开发，请联系我，如使用插件过程有疑问或者建议，请加我们的QQ技术交流群：884210423

【预告】基于相同原理，目前可视化低代码开发插件也在开发中，如感兴趣请入上面的QQ群